package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {

	@Test
	public void testLargestFirst() throws Exception {
		int first= new MinMax().largest(10,5);
		assertEquals("First", 10, first);
	}
	
	@Test
	public void testLargestSecond() throws Exception {
		int second= new MinMax().largest(10,20);
		assertEquals("First", 20, second);
	}
}
