package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class AboutCPDOFTest {

	@Test
	public void testDuration() throws Exception {
		String testString = new AboutCPDOF().desc();
		assertTrue("Testing AboutCPDOF Class",testString.contains("CP-DOF certification"));
	}
}
