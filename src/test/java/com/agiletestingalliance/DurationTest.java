package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class DurationTest {

	@Test
	public void testDuration() throws Exception {
		String testString = new Duration().dur();
		assertTrue("Testing Duration Class",testString.contains("CP-DOF"));
	}
}
